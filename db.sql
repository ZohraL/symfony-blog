DROP DATABASE IF EXISTS blog; 
CREATE DATABASE blog;
USE blog; 
CREATE TABLE post(
    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(64),
    author VARCHAR(64), 
    content TEXT,
    postDate DATETIME,
    imgPath TEXT
);

INSERT INTO post (title, author, content, postDate, imgPath) VALUES ('Bienvenue', 'Zohra', 'Welcome on Board', '2019-10-29', "https://images.unsplash.com/photo-1527833296831-2dcbf098d66f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80");
INSERT INTO post (title, author, content, postDate, imgPath) VALUES ('Activités Montessori', 'Maria', 'Une liste d''activités en fonction de l''age de votre enfant', '2018-08-28', "https://cdn.pixabay.com/photo/2014/10/25/21/21/blocks-503109_960_720.jpg");
INSERT INTO post (title, author, content, postDate, imgPath) VALUES ('Pics de croissance', 'Eudes', 'Les pics de croissance de 0 à 1 an', '2019-11-17', "https://get.pxhere.com/photo/hand-person-blur-growth-woman-flower-petal-love-finger-spring-red-color-child-hanging-colorful-pink-toy-baby-close-up-art-indoors-mother-daylight-infant-bright-organ-sweetness-emotion-adult-macro-photography-breastfeeding-cuddles-sense-baby-toy-920618.jpg");