<?php

namespace App\Repository;

use App\Entity\Post;
use Exception;
// le repo fait le lien avec la BDD


class PostRepository
{


    private $pdo;
    public function __construct()
    {
        $this->pdo = new \PDO(
            "mysql:host=" . $_ENV["DATABASE_HOST"] . ";dbname=" . $_ENV["DATABASE_NAME"],
            $_ENV["DATABASE_USERNAME"],
            $_ENV["DATABASE_PASSWORD"],
            [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION]

        );
    }
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM post');
        $query->execute();
        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $line) {
            $post = $this->sqlToPost($line);
            $list[] = $post;
        }

        return $list;
    }

    public function add(Post $post): void
    {

        $query = $this->pdo->prepare('INSERT INTO post (title, author, content, postDate, imgPath) VALUES (:title,:author,:content, :postDate, :imgPath)');

        $query->bindValue('title', $post->getTitle(), \PDO::PARAM_STR); // sécurise le form pour éviter les requêtes SQL cachées et le STR fait en sorte d'assigner dans le formulaire une valeur en string uniquement.
        $query->bindValue('author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue('content', $post->getContent(), \PDO::PARAM_STR);
        $query->bindValue('postDate', $post->getPostDate()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
        $query->bindValue('imgPath', $post->getimgPath(), \PDO::PARAM_STR);


        $query->execute();
        $post->setId(intval($this->pdo->lastInsertId()));
    }


    public function findById(int $id): ?Post
    {
        $query = $this->pdo->prepare('SELECT * FROM post WHERE id=:idPlaceholder');
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if ($line) {
            return $this->sqlToPost($line);
        }
        return null;
    }
    private function sqlToPost(array $line): Post
    {

        $resultDate = \DateTime::createFromFormat('Y-m-d H:i:s', $line['postDate']);
        return new Post($line['title'], $line['author'], $line['content'], $resultDate, $line['imgPath'], $line['id']);
    }

    public function deleteArticle(int $id)
    {

        $delete = $this->pdo->prepare('DELETE FROM post WHERE id=:id');
        $delete->bindValue('id', $id, \PDO::PARAM_INT);
        $delete->execute();
    }

    // public function updateArticle($id, $title, $author, $content, $imgPath)
    // {

    //     $update = $this->pdo->prepare('UPDATE post SET title=:title AND author=:author AND content=:content AND imgPath=:imgPath WHERE id=:id');
    //     $update->bindValue('title', $title, \PDO::PARAM_STR);
    //     $update->bindValue('author', $author, \PDO::PARAM_STR);
    //     $update->bindValue('content', $content, \PDO::PARAM_STR);
    //     $update->bindValue('imgPath', $imgPath, \PDO::PARAM_STR);
    //     $update->bindValue('id', $id, \PDO::PARAM_INT);

    //     $update->execute();
    // }



    // function compter_visite()
    // {


    //     global $pdo;
    //     $ip   = $_SERVER['REMOTE_ADDR'];
    //     $date = date('Y-m-d');


    //     $query = $pdo->prepare("
    //     INSERT INTO stats_visites (ip , date_visite , pages_vues) VALUES (:ip , :date , 1)
    //     ON DUPLICATE KEY UPDATE pages_vues = pages_vues + 1
    // ");

    //     $query->execute(array(
    //         ':ip'   => $ip,
    //         ':date' => $date
    //     ));
    // }
}
