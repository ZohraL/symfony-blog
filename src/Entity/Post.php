<?php

namespace App\Entity;

use DateTime;

class Post
{
    private $id;
    private $title;
    private $author;
    private $postDate;
    private $content;
    private $imgPath;

    public function __construct(string $title = '', string $author = '', string $content = '', \DateTime $postDate = null, ?string $imgPath ='', int $id = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->postDate = $postDate;
        $this->content = $content;
        $this->imgPath = $imgPath;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
    public function getAuthor(): ?string
    {
        return $this->author;
    }
    public function getContent(): ?string
    {
        return $this->content;
    }
    public function getPostDate(): ?\DateTime
    {
        return $this->postDate;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
    public function setAuthor(string $author): void
    {
        $this->author = $author;
    }
    public function setContent(string $content): void
    {
        $this->content = $content;
    }
    public function setpostDate(string $postDate): void
    {
        $this->postDate = $postDate;
    }

    public function getImgPath(): ?string
    {
        return $this->imgPath;
    }

    public function setImgPath(string $imgPath)
    {
        $this->imgPath = $imgPath;
    }
}
