<?php

namespace App\Controller;

use App\Entity\Post;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;





class BlogController extends AbstractController
{

    /**
     * @Route ("/", name="blog")
     */

    public function index(PostRepository $repo)
    {

        $postTab = $repo->findAll();

        dump($postTab);

        return $this->render('blog.html.twig', [
            'postTab' => $postTab
        ]);
    }


    /**
     * @Route ("add-article", name="add-article")
     */

    public function addArticle(Request $request)
    {
        $post = null;
        $title = $request->get("title");
        $author = $request->get("author");
        $content = $request->get("content");
        $imgPath = $request->get("imgPath");
        if ($title && $author && $content)/* && $imgPath)*/ {
            $post = new Post($title, $author, $content, new \DateTime(), $imgPath);
            dump($post);
            $postRepository = new PostRepository();
            $postRepository->add($post);
            return $this->redirectToRoute('blog');
        }

        return $this->render('add-article.html.twig', [
            'post' => $post
        ]);
    }


    /**
     *  @Route("/post/{id}", name="one_post")
     */
    public function findArticle(int $id)
    {
        $repo = new PostRepository();
        $post = $repo->findById($id);
        return $this->render('one-post.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route("/del/{id}", name="deletepost")
     */
    public function delete(int $id)
    {
        $repo = new PostRepository();
        $repo->deleteArticle($id);
        return $this->redirectToRoute('blog');
    }
}
