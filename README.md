Le projet Blog est un projet à réaliser en utilisant le framework Symfony. 

Pour ce projet, j'ai utilisé le langage PHP. J'ai crée une base de données en utilisant l'outil MariaDB.

Actuellement, mes fonctions (findAll, delete, findbyId, add) sont fonctionnelles.

En ce qui concerne l'interface, la page d'accueil affiche bien les articles figurant dans la base de données.

La création d'article se fait via un formulaire qui fonctionne mais rien ne s'enregistre en base de données. Ceci donnera lieu à une V2.

User Story: 

- en tant que lecteur, je souhaite lire des articles sur la parentalité positive afin d'élargir mes connaissances.
- en tant que rédacteur, je souhaite écrire des articles afin de partager mes connaissances.
- en tant qu'utilisateur, je souhaite écrire un article afin de le rajouter en base de données. 



Les maquettes sont dans le fichier "image" du projet.



